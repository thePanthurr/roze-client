package com.thepanthurr.client.module.toggles;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.client.gui.DrawUtils;
import com.thepanthurr.client.module.Toggle;
import com.thepanthurr.lib.event.EventMark;
import com.thepanthurr.lib.event.events.EventDraw;

/**
 * <b>ToggleWaterMark</b> is a <code>Module</code> that displays the clients
 * name and version in the top left of the screen, as to identify the client.
 *
 * @author thePanthurr
 * @since 7/14/13
 */
public class ToggleWaterMark extends Toggle {
    public ToggleWaterMark() {
        super("NONE");
        toggle();
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * A method used to display the Watermark on the screen every tick.
     *
     * @param eventDraw A parameter used to access the arguments of
     *                  the event's constructor.
     */
    @EventMark
    public void onUpdate(EventDraw eventDraw) {
        if (!isEnabled()) {
            return;
        }

        DrawUtils.drawStringWithSilhouette("\247d" + ClientImpl.getInstance().getVersion() + "\247f_\247d"
                        + ClientImpl.getInstance().getName().substring(0, 1) + "\247f"
                        + ClientImpl.getInstance().getName().substring(1, ClientImpl.getInstance().getName().length()), 2, 2, 0xFFFFFF);
    }
}
