package com.thepanthurr.client.module.toggles;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.client.module.Toggle;
import com.thepanthurr.lib.event.EventMark;
import com.thepanthurr.lib.event.events.EventPreUpdate;
import com.thepanthurr.lib.value.NumberValue;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

/**
 * <b>ToggleAura</b> is a PvP modification to Minecraft used in order to
 * defend yourself from an onslaught of players or to kill many people
 * over and over again.
 *
 * @author thePanthurr
 * @since 8/3/13
 */
public class ToggleAura extends Toggle {
    private long start = 0L; //A variable used to measure the amount of time since the preUpdateEvent method has been run.
    private long end = -1L;  //A variable used to hold the time since the last attack.

    public ToggleAura() {
        super("F");
        valueArray.put("range", new NumberValue(3.5F, .15F));
        valueArray.put("delay", new NumberValue(73L, 5L));
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * Running before every motion update, it fixes the camera to the nearest
     * player, and then hits them repeatedly.
     *
     * @param eventPreUpdate A parameter used to access the arguments of
     *                       the event's constructor.
     */
    @EventMark
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (!isEnabled()) {
            return;
        }

        if (getNearestEntity() == null) {
            return;
        }

        EntityPlayer fighter = getNearestEntity();

        start = System.nanoTime() / 1000000;

        if (aimAtEntity(fighter)) {
            if (start - end >= (1000 / (long) Float.parseFloat(valueArray.get("delay").getValue().toString()) + 2)) {
                ClientImpl.getInstance().getWrapper().getPlayer().swingItem();
                ClientImpl.getInstance().getWrapper().getController()
                        .attackEntity(ClientImpl.getInstance().getWrapper().getPlayer(), fighter);
                end = System.nanoTime() / 1000000;
            }
        }
    }

    /**
     * Makes the player face the given other entityPlayer by calculating the yaw
     * and pitch in order to do so.
     *
     * @param entityPlayer is the person that the aimbot is supposed to aimbot.
     */
    private boolean aimAtEntity(EntityPlayer entityPlayer) {
        double xDifference = entityPlayer.posX - ClientImpl.getInstance().getWrapper().getPlayer().posX;
        double yDifference = (ClientImpl.getInstance().getWrapper().getPlayer().posY
                + ClientImpl.getInstance().getWrapper().getPlayer().getEyeHeight())
                - (entityPlayer.posY + entityPlayer.getEyeHeight());
        double zDifference = entityPlayer.posZ - ClientImpl.getInstance().getWrapper().getPlayer().posZ;

        float newYaw = (float) Math.toDegrees(Math.atan2(zDifference, xDifference)) - 90;
        float newPitch = (float) Math.toDegrees(Math.atan2(yDifference,
                Math.sqrt(xDifference * xDifference + zDifference * zDifference)));

        float curYaw = ClientImpl.getInstance().getWrapper().getPlayer().rotationYaw;
        float curPitch = ClientImpl.getInstance().getWrapper().getPlayer().rotationPitch;

        ClientImpl.getInstance().getWrapper().getPlayer().rotationYaw =
                updateRotation(curYaw, newYaw, 57);
        ClientImpl.getInstance().getWrapper().getPlayer().rotationYawHead =
                updateRotation(curYaw, newYaw, 57);
        ClientImpl.getInstance().getWrapper().getPlayer().rotationPitch =
                updateRotation(curPitch, newPitch, 57);

        return (Math.abs(MathHelper.wrapAngleTo180_float(newYaw) - MathHelper.wrapAngleTo180_float(curYaw)) <= 20)
                && (Math.abs(MathHelper.wrapAngleTo180_float(newPitch) - MathHelper.wrapAngleTo180_float(curPitch)) <= 20);
    }

    /**
     * Used to recieve the closest player to 'thePlayer' in order to
     * serve as a prioritization on who to hit first when scanning for
     * players to hit.
     *
     * @return the nearest EntityPlayer.
     */
    private EntityPlayer getNearestEntity() {
        float distance = (Float) valueArray.get("range").getValue();
        EntityPlayer currentEntity = null;

        for (Object object : ClientImpl.getInstance().getWrapper().getWorld().playerEntities) {
            if (!(object instanceof EntityPlayer)) {
                continue;
            }

            EntityPlayer entityPlayer = (EntityPlayer) object;

            if (entityPlayer == ClientImpl.getInstance().getWrapper().getPlayer()) {
                continue;
            }

            if (entityPlayer.deathTime > 0) {
                continue;
            }

            if (ClientImpl.getInstance().getWrapper().getPlayer().getDistanceToEntity(entityPlayer) >= distance) {
                continue;
            }

            distance = ClientImpl.getInstance().getWrapper().getPlayer().getDistanceToEntity(entityPlayer);
            currentEntity = entityPlayer;
        }

        return currentEntity;
    }

    /**
     * A method used to turn the camera by increments so that the Aura can bypass
     * the NoCheatPlus plugin.
     *
     * @param curRotation  The rotation of the player.
     * @param endRotation  The rotation that the player is supposed to end up at.
     * @param maxIncrement The amount that the player can turn at a time.
     * @return The new rotation that the current rotation should be set to.
     */
    private float updateRotation(float curRotation, float endRotation, float maxIncrement) {
        float wrapRotation = MathHelper.wrapAngleTo180_float(endRotation - curRotation);

        if (wrapRotation > maxIncrement) {
            wrapRotation = maxIncrement;
        }

        if (wrapRotation < -maxIncrement) {
            wrapRotation = -maxIncrement;
        }

        return curRotation + wrapRotation;
    }
}
