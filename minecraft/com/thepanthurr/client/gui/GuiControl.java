package com.thepanthurr.client.gui;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.client.module.Toggle;
import com.thepanthurr.lib.module.Module;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;

import java.util.ArrayList;

/**
 * <b>GuiControl</b> is a class used to manage the drawing of
 * the buttons to the screen.
 *
 * @author thePanthurr
 * @since 9/14/13
 */
public class GuiControl extends GuiScreen {
    private int columns = 4;                                    //The amount of columns in the grid of Buttons.
    private int rows = 100;                                     //The amount of rows in the grid, might handle max rows differently in future.
    private Button[][] buttonList = new Button[columns][rows];  //The array holding the positions of each button, goes from [0][0] to [maxColumns][100].
    private int curRow = 0;                                     //The current row that the button adder is looping through, used to track positioning.
    private int curColumn = 0;                                  //The current column that the button adder is looping through, used to track positioning.

    /**
     * Used to register a button for each viable Module.
     */
    public void addButtons() {
        buttonList = new Button[columns][rows];

        for (int column = 0; column < ClientImpl.getInstance().getFactory().getModuleControl()
                .getModules().size(); column++) {

            Module module = ClientImpl.getInstance().getFactory().getModuleControl()
                    .getModules().get(column);

            if (!(module instanceof Toggle)) {
                return;
            }

            Toggle toggle = (Toggle) module;

            if (curColumn >= columns) {
                curColumn = 0;
                curRow++;
            }

            buttonList[curColumn][curRow] = new Button(toggle);
            curColumn++;
        }

        curColumn = 0;
        curRow = 0;
    }

    @Override
    public void drawScreen(int i, int j, float f) {
        ScaledResolution scaledResolution = new ScaledResolution(Minecraft.getMinecraft(),
                ClientImpl.getInstance().getWrapper().getDisplayWidth(), ClientImpl.getInstance().getWrapper().getDisplayHeight());

        columns = ((scaledResolution.getScaledWidth() - 4) / (Button.WIDTH + 4)) > 0
                ? ((scaledResolution.getScaledWidth() - 4) / (Button.WIDTH + 4))
                : 1;

        addButtons();

        for (int loopColumn = 0; loopColumn < columns; loopColumn++) {
            for (int loopRow = 0; loopRow < rows; loopRow++) {
                Button curButton = buttonList[loopColumn][loopRow];

                if (curButton == null) {
                    continue;
                }

                curButton.setPositions(2 + (Button.WIDTH + 4) * loopColumn, 2 + (Button.HEIGHT + 4) * loopRow);
                curButton.onDraw(i, j);
            }
        }
    }

    @Override
    public void mouseClicked(int i, int j, int k) {
        for (Button button : getButtons()) {
            button.onClick(i, j, k);
        }
    }

    @Override
    public void mouseMovedOrUp(int i, int j, int k) {
        for (Button button : getButtons()) {
            button.onRelease(i, j, k);
        }
    }

    /**
     * A function to speed up looping through the buttons.
     *
     * @return an <code>ArrayList</code> of the type <code>Button</code>.
     */
    public ArrayList<Button> getButtons() {
        ArrayList<Button> buttons = new ArrayList<Button>();
        for (int loopColumn = 0; loopColumn < columns; loopColumn++) {
            for (int loopRow = 0; loopRow < rows; loopRow++) {
                Button curButton = buttonList[loopColumn][loopRow];

                if (curButton == null) {
                    continue;
                }

                buttons.add(curButton);
            }
        }

        return buttons;
    }
}
