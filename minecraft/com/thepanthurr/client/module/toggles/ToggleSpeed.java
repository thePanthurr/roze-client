package com.thepanthurr.client.module.toggles;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.client.module.Toggle;
import com.thepanthurr.lib.event.EventMark;
import com.thepanthurr.lib.event.events.EventPreUpdate;

/**
 * <b>ToggleSpeed</b> is a Module used for automating sprinting in
 * Minecraft.
 *
 * @author thePanthurr
 * @since 7/19/13
 */
public class ToggleSpeed extends Toggle {
    public ToggleSpeed() {
        super("V");
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {
        ClientImpl.getInstance().getWrapper().getPlayer().setSprinting(false);
    }

    /**
     * A method used to set the player's sprinting status to true before
     * each motion update is sent.
     *
     * @param eventPreUpdate A parameter used to access the arguments of
     *                       the event's constructor.
     */
    @EventMark
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (!isEnabled()) {
            return;
        }

        if (ClientImpl.getInstance().getWrapper().getPlayer().movementInput.moveForward > 0
                && !ClientImpl.getInstance().getWrapper().getPlayer().isCollidedHorizontally
                && !ClientImpl.getInstance().getWrapper().getPlayer().isSwingInProgress) {
            ClientImpl.getInstance().getWrapper().getPlayer().setSprinting(true);
        }
    }
}
