package com.thepanthurr.client.event;

import com.thepanthurr.lib.event.Event;
import com.thepanthurr.lib.event.EventControl;
import com.thepanthurr.lib.event.EventListener;
import com.thepanthurr.lib.event.EventMark;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <b>EventControlImpl</b> is an implementation of <code>EventControl</code> that utilizes
 * reflection and caching methods in order to use a non-resource intensive Annotation-based
 * method calling system.
 *
 * @author thePanthurr
 * @since 6/20/13
 */
public class EventControlImpl implements EventControl {
    public Map<EventListener, List<Method>> listenerIndex = new HashMap<EventListener, List<Method>>(); //A HashMap holding the EventListener classes and a list of the Annotated methods.

    @Override
    public void indexListener(EventListener eventListener) {
        ArrayList<Method> methodIndex = new ArrayList<Method>();

        for (Method method : eventListener.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(EventMark.class)
                    && method.getParameterTypes().length == 1) {
                methodIndex.add(method);
            }
        }

        listenerIndex.put(eventListener, methodIndex);
    }

    @Override
    public void removeListener(EventListener eventListener) {
        listenerIndex.remove(eventListener);
    }

    @Override
    public synchronized Event fireEvent(final Event event) {
        Map<EventListener, Method> methodMap = new HashMap<EventListener, Method>();

        for (Entry<EventListener, List<Method>> entry : listenerIndex.entrySet()) {
            for (Method method : entry.getValue()) {
                if (method.getParameterTypes()[0].equals(event.getClass())) {
                    methodMap.put(entry.getKey(), method);
                }
            }
        }

        for (Entry<EventListener, Method> entry : methodMap.entrySet()) {
            try {
                entry.getValue().invoke(entry.getKey(), event);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        return event;
    }

}
