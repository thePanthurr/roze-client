package com.thepanthurr.client;

import com.thepanthurr.client.containers.FactoryImpl;
import com.thepanthurr.client.containers.WrapperImpl;
import com.thepanthurr.lib.Client;
import com.thepanthurr.lib.module.Module;

/**
 * <b>ClientImpl</b> is an implementation of the <code>Client</code>
 * interface that shapes the name of the <code>Client</code> as well
 * as laying out basic functionality.
 *
 * @author thePanthurr
 * @since 6/20/13
 */
public final class ClientImpl implements Client {
    private static volatile ClientImpl newInstance;  //A variable for holding the single instance of ClientImpl.
    private FactoryImpl factory = new FactoryImpl(); //A variable for holding the current implementation of the Factory interface.
    private WrapperImpl wrapper = new WrapperImpl(); //A variable for holding the current implementation of the Wrapper interface.

    /**
     * A Singleton method used to access the single instance of
     * ClientImpl, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type ClientImpl, only set to be other
     *         than null when this method is first called.
     */
    public static ClientImpl getInstance() {
        ClientImpl result = newInstance;
        if (result == null) {
            synchronized (ClientImpl.class) {
                result = newInstance;
                if (result == null) {
                    result = new ClientImpl();
                    newInstance = result;
                }
            }
        }
        return newInstance;
    }

    /**
     * MAKE SURE YOU RUN THIS WHEN YOU CALL EVENT START OR THE CLIENT WONT WORK AT ALL Q_Q
     */
    @Override
    public void init() {
        getFactory().getEventControl().indexListener(getFactory().getModuleControl());
        getFactory().getModuleControl().addModules();
        for (Module module : getFactory().getModuleControl().getModules()) {
            System.out.println("The " + module.getName() + " module has been added to the client.");
        }
        getFactory().getGuiControl().addButtons();
    }

    @Override
    public String getName() {
        return "Roze";
    }

    @Override
    public String getVersion() {
        return "a2";
    }

    @Override
    public FactoryImpl getFactory() {
        return factory;
    }

    @Override
    public WrapperImpl getWrapper() {
        return wrapper;
    }
}

