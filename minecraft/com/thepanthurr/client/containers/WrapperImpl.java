package com.thepanthurr.client.containers;

import com.thepanthurr.lib.containers.Wrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.settings.GameSettings;

/**
 * <b>WrapperImpl</b> is a class utilizing the functionality
 * of <code>Wrapper</code> and returning the accurate implementations
 * of the <code>Minecraft</code> variables.
 *
 * @author thePanthurr
 * @since 7/12/13
 */
public class WrapperImpl implements Wrapper {
    @Override
    public EntityClientPlayerMP getPlayer() {
        return Minecraft.getMinecraft().thePlayer;
    }

    @Override
    public WorldClient getWorld() {
        return Minecraft.getMinecraft().theWorld;
    }

    @Override
    public PlayerControllerMP getController() {
        return Minecraft.getMinecraft().playerController;
    }

    @Override
    public NetHandlerPlayClient getQueue() {
        return Minecraft.getMinecraft().getNetHandler();
    }

    @Override
    public GameSettings getSettings() {
        return Minecraft.getMinecraft().gameSettings;
    }

    @Override
    public FontRenderer getFont() {
        return Minecraft.getMinecraft().fontRenderer;
    }

    @Override
    public int getDisplayWidth() {
        return Minecraft.getMinecraft().displayWidth;
    }

    @Override
    public int getDisplayHeight() {
        return Minecraft.getMinecraft().displayHeight;
    }

    /**
     * Used to play sounds such as clicking for the GUI.
     *
     * @return the sound manager instance in Minecraft.java
     */
    public SoundHandler getSound() {
        return Minecraft.getMinecraft().getSoundHandler();
    }
}
