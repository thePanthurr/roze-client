package com.thepanthurr.client.module.toggles;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.client.module.Toggle;
import net.minecraft.client.Minecraft;

/**
 * <b>ToggleWaterMark</b> is a <code>Module</code> that displays the clients
 * name and version in the top left of the screen, as to identify the client.
 *
 * @author thePanthurr
 * @since 7/14/13
 */
public class ToggleGui extends Toggle {
    public ToggleGui() {
        super("UP");
    }

    @Override
    protected void onEnable() {
        toggle();
        Minecraft.getMinecraft()
                .displayGuiScreen(ClientImpl.getInstance().getFactory().getGuiControl());
    }

    @Override
    protected void onDisable() {

    }
}
