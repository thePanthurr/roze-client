package com.thepanthurr.client.containers;

import com.thepanthurr.client.event.EventControlImpl;
import com.thepanthurr.client.gui.GuiControl;
import com.thepanthurr.client.module.ModuleControlImpl;
import com.thepanthurr.lib.containers.Factory;

/**
 * <b>FactoryImpl</b> is a class used to get the instances of Client-specific
 * Controllers.
 *
 * @author thePanthurr
 * @since 6/25/13
 */
public class FactoryImpl implements Factory {
    private final EventControlImpl eventControl = new EventControlImpl();     //An instance of an EventController to be able to call events.
    private final ModuleControlImpl moduleControl = new ModuleControlImpl();  //An instance of a ModuleController to be able to load Toggles and Commands.
    private final GuiControl guiControl = new GuiControl();                   //A class designed to handle the creation of, as well as functionality of, Buttons.

    @Override
    public EventControlImpl getEventControl() {
        return eventControl;
    }

    @Override
    public ModuleControlImpl getModuleControl() {
        return moduleControl;
    }

    /**
     * Used in order to get the class that will be handling the
     * creation of buttons, windows, and other onscreen elements.
     *
     * @return the final instance of GuiControl in this class.
     */
    public GuiControl getGuiControl() {
        return guiControl;
    }
}
