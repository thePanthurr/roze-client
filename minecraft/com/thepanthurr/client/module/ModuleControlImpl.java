package com.thepanthurr.client.module;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.client.gui.DrawUtils;
import com.thepanthurr.client.module.toggles.*;
import com.thepanthurr.lib.event.EventListener;
import com.thepanthurr.lib.event.EventMark;
import com.thepanthurr.lib.event.events.EventDraw;
import com.thepanthurr.lib.event.events.EventKeyPressed;
import com.thepanthurr.lib.module.Module;
import com.thepanthurr.lib.module.ModuleControl;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>ModuleControlImpl</b> is an implementation of the <code>ModuleControl</code>
 * abstract class used to hold an ArrayList of <code>Toggle</code> <code>Module</code>s.
 *
 * @author thePanthurr
 * @since 7/14/13
 */
public class ModuleControlImpl extends ModuleControl implements EventListener {
    //TODO: Acquire knowledge of how to make gradient text.
    private List<Module> toggleList = new ArrayList<Module>(); //A list holding the Toggles that the Controller is currently handling.

    @Override
    public void addModules() {
        toggleList.add(new ToggleAura());
        toggleList.add(new ToggleBow());
        toggleList.add(new ToggleGui());
        toggleList.add(new ToggleSight());
        toggleList.add(new ToggleSpeed());
        toggleList.add(new ToggleWaterMark());
    }

    @Override
    public List<Module> getModules() {
        return toggleList;
    }

    /**
     * A method used to handle the batch keybinding of registered <code>Module</code>s.
     *
     * @param eventKeyPressed A parameter used to access the arguments of
     *                        the event's constructor.
     */
    @EventMark
    public void handleKeys(EventKeyPressed eventKeyPressed) {
        for (Module module : getModules()) {
            if (module instanceof Toggle) {
                Toggle toggle = (Toggle) module;
                if (Keyboard.getKeyIndex(toggle.getKey())
                        == eventKeyPressed.getKey()) {
                    toggle.toggle();
                }
            }
        }
    }

    /**
     * A method used to handle the batch drawing of registered <code>Module</code> names
     * to the screen.
     *
     * @param eventDraw A parameter used to access the arguments of
     *                  the event's constructor.
     */
    @EventMark
    public void handleArray(EventDraw eventDraw) {
        int yPosition = 2;
        ScaledResolution scaledResolution = new ScaledResolution(Minecraft.getMinecraft(),
                ClientImpl.getInstance().getWrapper().getDisplayWidth(), ClientImpl.getInstance().getWrapper().getDisplayHeight());

        for (Module module : getModules()) {
            if (module instanceof Toggle) {

                Toggle toggle = (Toggle) module;

                if (toggle.isEnabled()) {
                    DrawUtils.drawStringWithSilhouette("\247d" + toggle.getName().substring(0, 1)
                                    + "\247f" + toggle.getName().substring(1, toggle.getName().length()),
                            scaledResolution.getScaledWidth() - ClientImpl.getInstance().getWrapper().getFont().getStringWidth(toggle.getName()) - 2,
                            yPosition, 0xFFFFFF);
                    yPosition += 10;
                }
            }
        }
    }
}
