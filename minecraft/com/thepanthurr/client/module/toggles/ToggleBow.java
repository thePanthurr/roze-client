package com.thepanthurr.client.module.toggles;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.client.module.Toggle;
import com.thepanthurr.lib.event.EventMark;
import com.thepanthurr.lib.event.events.EventPreUpdate;
import com.thepanthurr.lib.value.NumberValue;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.MathHelper;

/**
 * <b>ToggleBow</b> is a PvP modification designed to make aiming a bow easier
 * by doing it for you :) Optional movement prediction.
 *
 * @author thePanthurr
 * @since 8/10/13
 */
public class ToggleBow extends Toggle {
    public ToggleBow() {
        super("G");
        valueArray.put("range", new NumberValue(50, 1));
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    @EventMark
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        EntityPlayer fighter = getNearestCEntity(90);

        if (!isEnabled()) {
            return;
        }

        if (!ClientImpl.getInstance().getWrapper().getPlayer().isUsingItem()) {
            return;
        }

        if (Item.itemRegistry.getIDForObject(ClientImpl.getInstance().getWrapper()
                .getPlayer().getCurrentEquippedItem().getDisplayName().toLowerCase().replace(" ", "_"))
                != Item.itemRegistry.getIDForObject("bow")) {
            return;
        }

        ClientImpl.getInstance().getWrapper().getPlayer().rotationYaw = getYaw(fighter);
        ClientImpl.getInstance().getWrapper().getPlayer().rotationYawHead = getYaw(fighter);
        ClientImpl.getInstance().getWrapper().getPlayer().rotationPitch = getPitch(fighter);
    }

    /**
     * This method return the closest Entity
     * to your cursor.
     *
     * @param maxRotation The largest rotation that the player is allowed
     *                    to turn.
     * @return The entity that is closest to your camera.
     */
    private EntityPlayer getNearestCEntity(int maxRotation) {
        EntityPlayer currentEntity = null;
        double distance = maxRotation;

        for (Object object : ClientImpl.getInstance().getWrapper().getWorld().getLoadedEntityList()) {

            if (!(object instanceof EntityPlayer)) {
                continue;
            }

            EntityPlayer entityPlayer = (EntityPlayer) object;

            if (ClientImpl.getInstance().getWrapper().getPlayer().getDistanceToEntity(entityPlayer)
                    > Float.parseFloat(valueArray.get("range").getValue().toString())) {
                continue;
            }

            if (!ClientImpl.getInstance().getWrapper().getPlayer().canEntityBeSeen(entityPlayer)) {
                continue;
            }

            if (ClientImpl.getInstance().getWrapper().getPlayer() == entityPlayer) {
                continue;
            }

            double curDistance = getDistanceBetweenAngles(getYaw(entityPlayer), ClientImpl.getInstance().getWrapper().getPlayer().rotationYaw);

            if (curDistance < distance) {
                currentEntity = entityPlayer;
                distance = curDistance;
            }
        }
        return currentEntity;
    }

    /**
     * This method determines the yaw needed to
     * look at an Entity.
     *
     * @param entityPlayer The entity which the method will
     *                     calculate the appropriate yaw for.
     * @return The way of determining the yaw to look at the
     *         entity.
     */
    private float getYaw(EntityPlayer entityPlayer) {
        double x = entityPlayer.posX - ClientImpl.getInstance().getWrapper().getPlayer().posX;
        double z = entityPlayer.posZ - ClientImpl.getInstance().getWrapper().getPlayer().posZ;

        return (float) Math.toDegrees(Math.atan2(z, x)) - 90;
    }

    /**
     * This method determines the pitch needed to
     * hit an Entity at position X,Y,Z with
     * a constant velocity and gravity.
     *
     * @param entityPlayer The entityPlayer which the method will
     *                     calculate the appropriate pitch for.
     * @return rangle
     *         The calculated angle for a low trajectory
     *         after determining the course of action when
     *         reaching an NaN value.
     */
    public float getPitch(EntityPlayer entityPlayer) {
        double xdist = entityPlayer.posX - ClientImpl.getInstance().getWrapper().getPlayer().posX;
        double ydist = (entityPlayer.posY + (entityPlayer.getEyeHeight() - 0.3) - ClientImpl.getInstance().getWrapper().getPlayer().posY);
        double zdist = entityPlayer.posZ - ClientImpl.getInstance().getWrapper().getPlayer().posZ;
        float dist = (float) Math.sqrt(xdist * xdist + zdist * zdist);
        float vel = getVelocity();
        float grav = 0.006F;
        float sqrt = (vel * vel * vel * vel) - (float) (grav * (grav * (dist * dist) + 2F * ydist * (vel * vel)));
        sqrt = MathHelper.sqrt_float(sqrt);
        float rangle = -(float) Math.toDegrees(Math.atan(((vel * vel) - sqrt) / (grav * dist)));
        if (Float.isNaN(rangle)) {
            rangle = (float) (-Math.toDegrees(Math.atan(ydist / dist)));
        }
        return rangle;
    }

    /**
     * This method converts the angles to an angle less than 180.
     *
     * @param angle  The angle where the method starts counting.
     * @param angle1 The angle where the method stops counting.
     * @return The difference between the 2 angles made to be less than 180.
     */
    private float getDistanceBetweenAngles(float angle, float angle1) {
        float diff = (Math.abs(angle - angle1)) % 360;

        if (diff > 180) {
            diff = (360 - diff);
        }

        return diff;
    }

    /**
     * This method returns the current velocity
     * that the arrow would be shot at if the bow
     * was released.
     *
     * @return The variable that is equivalent to the
     *         initial velocity of the Arrow calculated using
     *         Minecraft's algorithm.
     */
    public float getVelocity() {
        float pow = ((float) ClientImpl.getInstance().getWrapper().getPlayer().getItemInUseDuration()) / 20f;
        pow = (pow * pow + pow * 2.0F) / 3.0F;

        if (pow < 0.1F) {
            pow = 0.1F;
        }
        if (pow > 1.0F) {
            pow = 1.0F;
        }

        return pow;
    }
}
