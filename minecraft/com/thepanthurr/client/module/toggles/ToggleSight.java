package com.thepanthurr.client.module.toggles;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.client.module.Toggle;
import net.minecraft.potion.PotionEffect;

/**
 * <b>ToggleSight</b> is a <code>Module</code> used to see things
 * in situations such as underground mining or at night time.
 *
 * @author thePanthurr
 * @since 7/14/13
 */
public class ToggleSight extends Toggle {
    public ToggleSight() {
        super("C");
    }

    @Override
    protected void onEnable() {
        ClientImpl.getInstance().getWrapper().getPlayer().addPotionEffect(new PotionEffect(16, 100000000, 0));
    }

    @Override
    protected void onDisable() {
        ClientImpl.getInstance().getWrapper().getPlayer().removePotionEffect(16);
    }
}
