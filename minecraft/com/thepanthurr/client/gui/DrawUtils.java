package com.thepanthurr.client.gui;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.lib.gui.GuiUtils;
import org.lwjgl.opengl.GL11;
import sun.org.mozilla.javascript.internal.ast.Yield;

import java.util.regex.Pattern;

/**
 * <b>DrawUtils</b> is a class used for referencing drawing methods in order
 * to create boxes on my screen.
 *
 * @author thePanthurr
 * @since 8/22/13
 */
public final class DrawUtils {
    /**
     * This method creates a rectangle on the screen.
     *
     * @param xPosition  the x coordinate where the left edge of the boxPosition
     *                   is to be aligned to. The top left of the screen is
     *                   0, 0).
     * @param yPosition  The y coordinate where the top edge of the boxPosition
     *                   is to be aligned to. The top left of the screen is
     *                   0, 0).
     * @param x1Position The x coordinate where the right edge of the box
     *                   is to be aligned to. The top left of the screen is
     *                   0, 0).
     * @param y1Position The y coordinate where the bottom edge of the box
     *                   is to be aligned to. The top left of the screen is
     *                   0, 0).
     * @param color      The hexadecimal color code that the rectangle will be
     *                   drawn in by default.
     */
    public static void drawRectangle(float xPosition, float yPosition,
                                     float x1Position, float y1Position, int color) {
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glPushMatrix();
        GL11.glColor4f(GuiUtils.getRGBFromHex(color)[0], GuiUtils.getRGBFromHex(color)[1],
                GuiUtils.getRGBFromHex(color)[2], GuiUtils.getRGBFromHex(color)[3]);
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glVertex2d(xPosition, yPosition);
        GL11.glVertex2d(xPosition, y1Position);
        GL11.glVertex2d(x1Position, y1Position);
        GL11.glVertex2d(x1Position, yPosition);
        GL11.glEnd();
        GL11.glPopMatrix();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_LINE_SMOOTH);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    /**
     * Draws a string with a black shadow with a light source from the top left.
     *
     * @param string    the string that is going to be drawn twice
     *
     * @param xPosition the x coordinate where the left edge of the string
     *                  is to be aligned to. The top left of the screen is
     *                  0, 0).
     *
     * @param yPosition The y coordinate where the top edge of the string
     *                  is to be aligned to. The top left of the screen is
     *                  0, 0).
     *
     * @param color     The hexadecimal color code that the string will be
     *                  drawn in by default.
     */
    public static void drawStringWithSilhouette(String string, int xPosition, int yPosition, int color) {
        ClientImpl.getInstance().getWrapper().getFont().drawString(stripColorCodes(string), xPosition + 1, yPosition + 1, 0x000000);
        ClientImpl.getInstance().getWrapper().getFont().drawString(string, xPosition, yPosition, color);
    }

    /**
     * This is a method that removes colors
     * from a string so that friends can be
     * saved accurately.
     *
     * @param string The string that is to have the color
     *               codes replaced.
     * @return string
     *         The string after using .replace to remove
     *         all the color codes.
     */
    public static String stripColorCodes(String string) {
        return Pattern.compile("(?i)\\u00A7[0-9A-FK-ORQ]").matcher(string).replaceAll("");
    }
}
