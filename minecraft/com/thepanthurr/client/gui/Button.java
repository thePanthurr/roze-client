package com.thepanthurr.client.gui;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.client.module.Toggle;
import com.thepanthurr.lib.gui.GuiElement;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.util.ResourceLocation;

import java.awt.*;

/**
 * <b>Button</b> is a class used to define what a button will look like in the GUI.
 *
 * @author thePanthurr
 * @since 8/21/13
 */
public class Button implements GuiElement {
    private Toggle toggle;               //Used to hold the current Toggle Module being used in the creation of the button for access to the name and toggle().
    private int bxPosition;              //Used to define the left x coordinate of the rectangle to appear.
    private int byPosition;              //Used to define the top left y coordinate of the rectangle to appear.

    public static final int WIDTH = 100; //Used to define how wide the Button appears in the GUI in pixels.
    public static final int HEIGHT = 40; //Used to define how tall the Button appears in the GUI in pixels.

    public Button(Toggle toggle) {
        this.toggle = toggle;
    }

    @Override
    public void onDraw(final int xPosition, final int yPosition) {
        DrawUtils.drawRectangle(bxPosition + 2, byPosition + 2, bxPosition + WIDTH + 2, byPosition + HEIGHT + 2, 0xFF000000);
        DrawUtils.drawRectangle(bxPosition, byPosition, bxPosition + WIDTH, byPosition + HEIGHT, 0xFFFF55FF);
        ClientImpl.getInstance().getWrapper().getFont().drawStringWithShadow(toggle.getName(),
                ((bxPosition + (bxPosition + WIDTH)) / 2) - (ClientImpl.getInstance().getWrapper().getFont().getStringWidth(toggle.getName()) / 2),
                ((byPosition + (byPosition + HEIGHT)) / 2) - (ClientImpl.getInstance().getWrapper().getFont().FONT_HEIGHT / 2), 0xFFFFFF);
        ClientImpl.getInstance().getWrapper().getFont().drawStringWithShadow(toggle.getKey().equals("NONE") ? "[]" : toggle.getKey(), bxPosition + 2, byPosition + 2, 0xFFFFFF);
    }

    @Override
    public void onClick(final int xPosition, final int yPosition, final int buttonPressed) {
        if (isHovered(xPosition, yPosition)) {
            ClientImpl.getInstance().getWrapper().getSound().playSound(PositionedSoundRecord.func_147674_a(new ResourceLocation("random.click"), 1.0F));
        }
    }

    @Override
    public void onRelease(final int xPosition, final int yPosition, final int buttonReleased) {
        if (isHovered(xPosition, yPosition)) {
            toggle.toggle();
        }
    }

    @Override
    public void onDrag(final int xPosition, final int yPosition) {

    }

    @Override
    public boolean isHovered(final int xPosition, final int yPosition) {
        return new Rectangle(bxPosition, byPosition, WIDTH, HEIGHT).contains(xPosition, yPosition);
    }

    /**
     * Used to define the pixel positions of each Button, so that they
     * can be accurately placed onto the screen.
     *
     * @param bxPosition The (b)uttons (xPosition) in pixels that the left side
     *                   should be drawn at.
     * @param byPosition The (b)uttons (yPosition) in pixels that the top side
     *                   should be drawn at.
     */
    public void setPositions(int bxPosition, int byPosition) {
        this.bxPosition = bxPosition;
        this.byPosition = byPosition;
    }
}
