package com.thepanthurr.client.module;

import com.thepanthurr.client.ClientImpl;
import com.thepanthurr.lib.event.EventListener;
import com.thepanthurr.lib.module.Module;

/**
 * <b>Toggel</b> is a class that defines basic on/off functionality as well
 * as a Key to do so for a Module.
 *
 * @author thePanthurr
 * @since 7/12/13
 */
public abstract class Toggle extends Module implements EventListener {
    private String key;      //A variable used to define what keybind will be used to enable and disable the module.
    private boolean enabled; //A variable holding the toggled state of the module.

    protected Toggle(String key) {
        this.key = key;
        ClientImpl.getInstance().getFactory().getEventControl().indexListener(this);
    }

    /**
     * A method that turns the <code>Module</code> on or off, and runs the corresponding
     * Method to let the Module do its activation or deactivation actions.
     */
    public void toggle() {
        enabled = !enabled;
        if (enabled) {
            onEnable();
        } else {
            onDisable();
        }
    }

    /**
     * A method that is called with the enabled variable has been set to <code>true</code>
     * via the <code>toggle()</code> method. Should be used for things that need to happen
     * before other events are called.
     */
    protected abstract void onEnable();

    /**
     * A method that is called with the enabled var4iable has been set to <code>true</code>
     * via the <code>toggle()</code> method. Should be used for things that need to happen
     * after other events are called and things that need to be returned to their original
     * state before the <code>Module</code> shuts down.
     */
    protected abstract void onDisable();

    /**
     * A method that is used by the ModuleController in order to handle the Keybind for
     * this Module.
     *
     * @return the current key that the Toggle is bound to.
     */
    public String getKey() {
        return key;
    }

    /**
     * A method that is used by the ModuleController in order to handle the Array for
     * the Modules.
     *
     * @return the current state of the <code>enabled</code> variable.
     */
    public boolean isEnabled() {
        return enabled;
    }
}
